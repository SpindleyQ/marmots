#!/usr/bin/env python2

# This file is part of MarMOTS.
#
# MarMOTS is free software: you can redistribute it and/or modify it under the terms of the GNU Affero 
# General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MarMOTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General 
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with MarMOTS.  If not, 
# see <https://www.gnu.org/licenses/>.
#
# Copyright 2009, 2010, 2011, 2020 Jeremy Penner

from whiteboard import *
import sys

def merge_whiteboards(runner1, runner2):
    runner1.gameLobby.rgproject.extend(runner2.gameLobby.rgproject)
    runner1.gameLobby.rgproject.sort(key=lambda p: p.DrawingLastModified().dtLastModified, reverse=True)
    return runner1

def merge_whiteboard_files(infile1, infile2, outfile):
    runner1 = tpers.Odb.Load(infile1)
    runner2 = tpers.Odb.Load(infile2)
    out = merge_whiteboards(runner1, runner2)
    tpers.Odb.Save(out, outfile)

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print('Usage:', sys.argv[0], 'infile1 infile2 outfile')
    else:
        merge_whiteboard_files(sys.argv[1], sys.argv[2], sys.argv[3])
