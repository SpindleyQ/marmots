{ pkgs ? import <nixpkgs> {}, shell ? false }:
let
    python = let
      packageOverrides = self: super: {
        # on nixos-20.09, mysqlclient is marked disabled when using python 2.7
        # disabled can't be overridden, so we recreate the package definition from scratch
        # https://github.com/NixOS/nixpkgs/issues/48663
        mysqlclient = super.buildPythonPackage rec {
          pname = "mysqlclient";
          version = "1.4.6";
          nativeBuildInputs = [pkgs.libmysqlclient];
          buildInputs = [pkgs.libmysqlclient];
          doCheck = false;
          src = super.fetchPypi {
            inherit pname version;
            sha256 = "f3fdaa9a38752a3b214a6fe79d7cae3653731a53e577821f9187e67cbecb2e16";
          };
        };
      };
    in pkgs.python2.override { inherit packageOverrides; self = python; };
    pyenv = pypkgs: with pypkgs; [
        greenlet twisted cython mysqlclient bcrypt
    ];
    buildInputs = [ (python.withPackages pyenv) ];
in
if shell then
  pkgs.mkShell { inherit buildInputs; }
else
  pkgs.stdenv.mkDerivation {
      name = "marmots";
      src = ./.;
      inherit buildInputs;
      installPhase = ''
        mkdir $out
        cp -R * $out
      '';
  }
