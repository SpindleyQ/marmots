# This file is part of MarMOTS.
#
# MarMOTS is free software: you can redistribute it and/or modify it under the terms of the GNU Affero 
# General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MarMOTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General 
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with MarMOTS.  If not, 
# see <https://www.gnu.org/licenses/>.
#
# Copyright 2009, 2010, 2011, 2020 Jeremy Penner

from engine import *
from basetoken import *
import telnet

class IntroTerm(Terminal):
    def run(self):
        self.newLine(3, 300)
        self.type("Logon Please: ")
        name = self.getLine()
        self.newLine(2, 300)
        self.typeLn("Welcome, " + name)
        self.type("You have ")
        self.wait(200)
        self.typeLn("top security clearance.")
        self.type("You are looking ")
        self.wait(200)
        self.typeLn("good to-day.")
        while True:
            self.typeLn("Would you like to download")
            self.type("all secret files? [Y/N] ")
            if (self.getPrintableKey().upper() == "Y"):
                break
            self.printSt("No")
            self.newLine()
            self.typeLn("But thou must!")
            self.wait(500)
        self.printSt("Yessir!")
        self.newLine(2, 50)
        self.type("Downloading... ")
        spinner = self.newSpinner()
        self.wait(3000)
        spinner.die()
        blnkScrn = Blinker(self.game, self.setColBg, [ansi.RED, ansi.BLUE])
        self.newLine(2, 50)
        self.typeLn("EXPERT HAX0R DETECTED")
        self.typeLn("CODE RED CODE RED")
        self.newLine(2, 400)
        self.wait(500)
        self.typeLn("OH SHIIIIIIIIIIIIIIIIT!")
        self.wait(800)
        blnkScrn.die()
        self.setColBg(ansi.BLACK)
        self.newLine(2, 50)
        self.printSt("NO CARRIER")
        self.newLine()
        self.client.quit()

class HaxorGame(Game):
    def GetRgclsTokTrans(self):
        return [[AutoJoiner, IntroTerm]]

class HaxorRunner(Runner):
    def RunGame(self, client):
        client.joinGame(HaxorGame())

if __name__ == "__main__":
    HaxorRunner().RunServer()
