#!/bin/bash

fnnew="whiteboard.marm.`date "+%F_%H-%M"`"

cd /home/jeremy/src/marmots

save ()
{
  echo save | telnet localhost 20002 &> /dev/null
  sleep 5 # wait for save to complete
}

backup ()
{
    cp whiteboard.marm $fnnew
    gzip $fnnew
}

if [ -e whiteboard.marm ]
then
  cp whiteboard.marm whiteboard.marm.old
  save
  cmp whiteboard.marm whiteboard.marm.old &> /dev/null 
  if [ $? != 0 ] 
  then
    backup
  fi
  rm whiteboard.marm.old
else
  save
  backup
fi

