# This file is part of MarMOTS.
#
# MarMOTS is free software: you can redistribute it and/or modify it under the terms of the GNU Affero 
# General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MarMOTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General 
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with MarMOTS.  If not, 
# see <https://www.gnu.org/licenses/>.
#
# Copyright 2009, 2010, 2011, 2020 Jeremy Penner

import json
import os

def override(fn):
    with open(fn, 'rt') as f:
        for k, v in json.load(f).iteritems():
            globals()[k] = v

# Default configuration options follow - they can be overridden with a JSON file

# Port to run the telnet server on.
PORT_TELNET = 19999
# Port to run the command server on. Used to trigger saving.
PORT_CMDSERVER = 20002
# Hostname to bind servers on.  Leave as "" for all.
BIND_HOSTNAME = ""

# width and height; default 80x25
W = 80
H = 25
# directory to save ANSI pictures to. leave as "." for current directory.  no trailing slash required. "" == /
DIR_ANSI = "."

# authorization.  Options are "Noop" (make up a username, no password), "Drupal" (fetches from MySQL Drupal database), 
#"DrupalBcrypt" (fetches from MySQL Drupal database hacked to bcrypt the md5 hash - basically just Glorious Trainwrecks)
AUTH = "Noop"
# if Drupal, configure these options:
#drupaldb = "database"
#drupaldb_user = "user"
#drupaldb_password = "password"
#drupaldb_host = "localhost"

# Path to the object database to load / save
ODBPATH = "whiteboard.marm"
