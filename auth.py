# This file is part of MarMOTS.
#
# MarMOTS is free software: you can redistribute it and/or modify it under the terms of the GNU Affero 
# General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MarMOTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General 
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with MarMOTS.  If not, 
# see <https://www.gnu.org/licenses/>.
#
# Copyright 2009, 2010, 2011, 2020 Jeremy Penner

import MySQLdb
import hashlib
import config
import bcrypt
import traceback

class NoopAuth(object):
    def FAuthorized(self, user, passwd):
        return True

class DrupalAuth(object):
    def FHashMatches(self, md5, dbhash):
        return md5 == dbhash
    def FAuthorized(self, user, passwd):
        conn = MySQLdb.connect(host = config.drupaldb_host, user = config.drupaldb_user, passwd = config.drupaldb_password, db = config.drupaldb)
        try:
            cursor = conn.cursor()
            try:
                firsthash = hashlib.md5(passwd).hexdigest()
                cursor.execute("SELECT pass FROM users WHERE name = %s", (user,))
                row = cursor.fetchone()
                return row and self.FHashMatches(firsthash, row[0])
            finally:
                cursor.close()
        except Exception:
            traceback.print_exc()
            return False
        finally:
            conn.close()

class DrupalBcryptAuth(DrupalAuth):
    def FHashMatches(self, md5, dbhash):
        return bcrypt.checkpw(md5, dbhash)