# This file is part of MarMOTS.
#
# MarMOTS is free software: you can redistribute it and/or modify it under the terms of the GNU Affero 
# General Public License as published by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# MarMOTS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General 
# Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with MarMOTS.  If not, 
# see <https://www.gnu.org/licenses/>.
#
# Copyright 2009, 2010, 2011, 2020 Jeremy Penner

from basetoken import *
import ansi_cython as ansi

class LoginTerm(Terminal):
    def run(self):
        self.setColBg(ansi.BLUE)
        self.moveTo(0, 2)
        self.printSt("* * *   MarMOTS is licensed under the AGPL. You can run your own server    * * *")
        self.moveTo(0, 3)
        self.printSt("* * *   and make any modifications you like, but all users must be given   * * *")
        self.moveTo(0, 4)
        self.printSt("* * *   the opportunity to use your changes and run their own servers too! * * *")
        self.moveTo(0, 5)
        self.printSt("* * *   https://bitbucket.org/SpindleyQ/marmots                            * * *")
        self.newLine(3, 300)
        self.type("Logon Please: ")
        user = self.getLine()
        self.newLine()
        self.type("Password: ")
        self.chEcho = "*"
        passwd = self.getLine()
        self.chEcho = None
        self.newLine(2, 300)
        if self.client.login(user, passwd):
            self.typeLn("Welcome to MarMOTS, " + user)
            self.typeLn("I see you have driven here in your Ferrari.")
            self.typeLn("Logging you in...")
            self.wait(250)
            self.game.finish(True)
        else:
            self.setColBg(ansi.RED)
            self.typeLn("I am sorry, " + user + ", but you appear to be insufficiently rhinoceros.")
            self.typeLn("Please report back after joining the Klik of the Month Klub.")
            self.newLine(2,300)
            self.setColBg(ansi.BLACK)
            self.printSt("NO CARRIER")
            self.wait(200)
            self.game.finish(False)
