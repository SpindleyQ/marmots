# MarMOTS
## The MARvelous Multiplayer Online Telnet Server

### What is MarMOTS?
MarMOTS is a tool for making things with your friends, through the magic of 
telnet.

### What kind of things?
For now, ANSI pictures and animations. Someday, videogames. Imagine ZZT's board
editor as a BBS door game, running on a board with, like, 16 modems, and you'll
have some idea of where MarMOTS is heading. 

### Who wrote it and why?
MarMOTS was written by me, Jeremy Penner, mostly between 2009-2011, for the 
Glorious Trainwrecks community. I was frustrated with the inaccessibility of
tools for making games collaboratively. After some false starts, I had the
idea of radically simplifying the problem - using old MS-DOS textmode graphics,
having clients simply send keypresses and having all logic run on the server.
It was quickly embraced by the community and extended to become a reasonably
capable ANSI art creation tool.

Then I burned out on it for 9 years, and it went offline, largely ignored.

### How do I use it?
If you just want to check it out, you can find more information about the 
flagship instance at http://marmots.glorioustrainwrecks.com/ .

If you want to run your own server, well, I don't have good documentation for
that yet. I may never get to it. But if you're interested, let me know and I
will help you. jeremy@glorioustrainwrecks.com.

### What is the license?
It's the AGPLv3 or later - see COPYING for legal details. But basically, if you
make modifications, and run a public server, you must provide those 
modifications back to the public.

I've spent 11 years choosing a license, so don't bother contacting me to ask
why it's not MIT. MarMOTS belongs in the commons for all to enjoy.
